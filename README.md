# Visual Studio For Windows Code Snippets #

This is simply personal collection of custom snippets I have created to make my life a little bit easier. I'm primarily using VS on the Mac these days, so this collection is a bit smaller.

### Adding Snippets to Visual Studio For Windows
Microsoft has some nice documentation on creating and adding snippets. Look for the heading, ["To Add a Code Snippet to Visual Studio"](https://msdn.microsoft.com/en-us/library/ms165394.aspx).